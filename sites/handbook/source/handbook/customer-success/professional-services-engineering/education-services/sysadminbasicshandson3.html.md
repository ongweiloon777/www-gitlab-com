---
layout: handbook-page-toc
title: "GitLab System Admin Basics Hands On Guide- Lab 3"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab System Admin Basics course."
---
# GitLab System Admin Basics Hands On Guide- Lab 3
{:.no_toc}


## LAB 3- USE THE GITLAB ADMIN PANEL

### Change Visibility Settings 
1. Log into your GitLab Instance with your admin username: root and password you created in Lab 1.
2. On the main screen under the More tab at the top of the screen, click on **Admin Area** or click the Wrench icon.  
3. The first thing we want to do is adjust the project visibility settings.  
4. Scroll to the bottom of the left hand side bar and click **Settings** > **General**  
5. Under Visibility and access controls, click the **Expand** button.
6. Change the Default project visibility to **Internal** by clicking on the radio button next to it. 
7. Repeat this step for the Default group visibility.  

### Update Sign-In Settings 
1. The second setting we want to update are the Sign-In restrictions. 
2. Still under the **Settings > Genera**l menu, click the **Collapse** button next to Visibility and access controls.  
3. Under Sign-in restrictions, click the **Expand** button. 
4. Under Two- factor authentication, click the checkbox next to **Require all users to set up Two-factor authentication**.  
5. Click the **Collapse** button next to Sign-in restrictions to close the menu.

### Update the Header Logo 
1. The last change we want to make in the admin area is to upload a header logo.  
2. On the left hand side panel, click on **Appearance**. 
3. Under the Navigation Bar section, click the **Choose File** button to upload your logo for the Header.  
4. Select the appropriate logo from your computer and click Open.  

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab System Admin Basics- please submit your changes via Merge Request!

